#include "schrottkonto.h"

#ifndef CPP_SUCKS_SPARKONTO
#define CPP_SUCKS_SPARKONTO

class Sparkonto : public Schrottkonto {
    private:
        bool aufgeloest;

    public:
        Sparkonto() : aufgeloest(false) {
            kontostand = 100;
        }

        void gutschreiben(int betrag) {
            if (aufgeloest) {
                cerr << "[!] Keep your money, pal" << endl;
                return;
            }

            Schrottkonto::gutschreiben(betrag);
        }

        int auszahlen(int betrag) {
            return Schrottkonto::auszahlen(betrag, 100);
        }

        int aufloesen() {
            aufgeloest = true;
            return kontostand;
        }
};

#endif