#include <iostream>
#include "girokonto.h"
#include "tagesgeldkonto.h"
#include "sparkonto.h"
#include "eidpliste.h"


int main(void) {
    Girokonto giro = Girokonto();
    Sparkonto spar = Sparkonto();
    Tagesgeldkonto tages = Tagesgeldkonto(giro);

    giro.anzeigen();
    cout << endl;
    spar.anzeigen();
    cout << endl;
    tages.anzeigen();
    cout << endl;

    // Punkt 1
    cout << "-= PUNKT 1 =-" << endl;
    cout << "[*] Gutschreiben: 9 €" << endl;
    spar.gutschreiben(900);
    spar.anzeigen();
    cout << endl << "[*] Auszahlen 2 €" << endl;
    spar.auszahlen(200);
    spar.anzeigen();
    cout << endl << "[*] Auszahlen 20 €" << endl;
    spar.auszahlen(2000);
    spar.anzeigen();
    cout << endl;

    // Punkt 2
    cout << "-= PUNKT 2 =-" << endl;
    cout << endl << "[*] Gutschreiben 100 €" << endl;
    giro.gutschreiben(10000);
    giro.anzeigen();
    cout << endl << "[*] Auszahlen 2000 €" << endl;
    giro.auszahlen(200000);
    giro.anzeigen();
    cout << endl;

    // Punkt 3
    cout << "-= PUNKT 3 =-" << endl;
    cout << "[*] Gutschreiben 3000 €" << endl;
    giro.gutschreiben(300000);
    giro.anzeigen();
    cout << endl << "[*] Überweisen 2000 €" << endl;
    giro.ueberweisen(spar, 200000);
    giro.anzeigen();
    cout << endl << "[*] Überweisen 2000 €" << endl;
    giro.ueberweisen(tages, 200000);
    giro.anzeigen();
    cout << endl;

    // Punkt 4
    cout << "-= PUNKT 4 =-" << endl;
    cout << "[*] Abbuchen 2000 €" << endl;
    tages.umbuchen(200000);
    tages.anzeigen();
    cout << endl;

    // Aufgabe f)
    Liste<Bankkonto> list = Liste<Bankkonto>();
    list.append(giro);
    list.append(spar);
    list.append(tages);

    cout << "-= AUFGABE f) =-" << endl;
    // #define Praktikumsaufgaben behindert
    for (int i = 0; i < 3; i++) {
        list.elementAt(i).anzeigen();
        cout << endl;
    }
}