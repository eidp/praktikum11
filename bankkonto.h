#include <string>
#include <iostream>

#ifndef CPP_SUCKS_BANKKONTO
#define CPP_SUCKS_BANKKONTO

using namespace std;

class Bankkonto {
    private:
        unsigned long kontonummer;
        string inhaber;
    
    protected:
        int kontostand;

    public:
        Bankkonto() : kontostand(0) {

        }

        void gutschreiben(int betrag) {
            if (betrag < 0) {
                cerr << "[!] No nigga ain't stealing my money" << endl;
                return;
            }

            kontostand += betrag;
        }

        int getKontostand() const {
            return kontostand;
        }

        void anzeigen() const {
            cout << "[Konto Nr. " << kontonummer << "]" << endl;
            cout << "Inhaber: " << inhaber << endl;
            cout << "=> " << kontostand / 100 << " €" << endl;
        }

        unsigned long getKontonummer() const {
            return kontonummer;
        }
};

#endif