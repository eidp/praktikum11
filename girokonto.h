#include "bankkonto.h"
#include "schrottkonto.h"

#ifndef CPP_SUCKS_GIROKONTO
#define CPP_SUCKS_GIROKONTO


class Girokonto : public Schrottkonto {


    public:
        int ueberweisen(Bankkonto& zielKonto, int betrag) {
            int diff = auszahlen(betrag);
            zielKonto.gutschreiben(diff);

            return diff;
        }

        int auszahlen(int betrag) {
            return Schrottkonto::auszahlen(betrag, -1000 * 100);    // Euro -> Cent
        }
};

#endif