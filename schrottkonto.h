#include "bankkonto.h"

#ifndef CPP_SUCKS_SCHROTTKONTO
#define CPP_SUCKS_SCHROTTKONTO

class Schrottkonto : public Bankkonto {

    public:
        int auszahlen(int betrag, int min) {
            if (betrag < 0) {
                cerr << "[!] Wanna pay me for this or what?" << endl;
                return 0;
            }
            
            int diff = kontostand - betrag;
            if (diff <= min) {
                diff = min;
            }
            kontostand = diff;

            return kontostand - diff;
        }
};

#endif