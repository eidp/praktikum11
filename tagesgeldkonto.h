#include "schrottkonto.h"

#ifndef CPP_SUCKS_TAGESGELDKONTO
#define CPP_SUCKS_TAGESGELDKONTO

class Tagesgeldkonto : public Schrottkonto {
    private:
        Girokonto referenzKonto;
    
    public:
        Tagesgeldkonto(Girokonto& referenzKonto) : referenzKonto(referenzKonto) {

        }

        int umbuchen(int betrag) {
            int diff = Schrottkonto::auszahlen(betrag, 0);
            referenzKonto.gutschreiben(diff);

            return diff;
        }

        int auszahlen(int betrag) {
            return 0;
        };
};

#endif